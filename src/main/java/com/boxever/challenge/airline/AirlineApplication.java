package com.boxever.challenge.airline;

import com.boxever.challenge.airline.algorithms.ShortestPathFinder;
import com.boxever.challenge.airline.exceptions.InputException;
import com.boxever.challenge.airline.model.AirlineGraph;
import com.boxever.challenge.airline.model.AirlinePath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirlineApplication  implements CommandLineRunner {
    private ShortestPathFinder finder;
    private String source;
    private String dest;

    public AirlineApplication(@Value("${source:DUB}") String source,
                              @Value("${dest:LHR}") String dest,
                              @Autowired ShortestPathFinder shortestPathFinder){
        this.finder = shortestPathFinder;
        this.source = source;
        this.dest = dest;
        this.validateInput();
    }

    @Override
    public void run(String... args) throws Exception {
        AirlinePath path = this.finder.getShortestPath(source, dest);
        if (path.exists()) {
            System.out.println(path);
        } else {
            System.out.println("No path between " + this.source + " and " + this.dest);
        }
    }

    private void validateInput() throws InputException {
        if (!AirlineGraph.AIRPORTS.contains(source) || !AirlineGraph.AIRPORTS.contains(dest)) {
            throw new InputException("Source or destination unknowns");
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(AirlineApplication.class, args);
    }
}
