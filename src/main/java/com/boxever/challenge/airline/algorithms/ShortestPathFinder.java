package com.boxever.challenge.airline.algorithms;

import com.boxever.challenge.airline.model.AirlineGraph;
import com.boxever.challenge.airline.model.AirlinePath;
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShortestPathFinder {

    private BellmanFordShortestPath<String, DefaultWeightedEdge> calculator;

    public ShortestPathFinder(@Autowired AirlineGraph g) {
        this.calculator = new BellmanFordShortestPath<>(g.getGraph());
    }

    public AirlinePath getShortestPath(String source, String dest) {
        return new AirlinePath(this.calculator.getPath(source, dest));
    }
}
