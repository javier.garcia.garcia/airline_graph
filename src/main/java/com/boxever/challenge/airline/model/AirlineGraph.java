package com.boxever.challenge.airline.model;

import lombok.Getter;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.springframework.stereotype.Component;

import java.util.Set;

@Getter
@Component
public class AirlineGraph {

    public static final Set<String> AIRPORTS = Set.of("DUB",
            "LHR",
            "CDG",
            "BOS",
            "BKK",
            "ORD",
            "LAS",
            "NYC",
            "LAX",
            "SYD");


    private SimpleWeightedGraph<String, DefaultWeightedEdge> graph;

    public AirlineGraph() {
        this.graph = buildEmptyAirlineGraph();
        this.fillGraph(graph);
    }

    private SimpleWeightedGraph<String, DefaultWeightedEdge> buildEmptyAirlineGraph() {
        return (SimpleWeightedGraph)GraphTypeBuilder.<String, DefaultWeightedEdge>undirected()
                .allowingMultipleEdges(false)
                .allowingSelfLoops(false)
                .edgeClass(DefaultWeightedEdge.class)
                .weighted(true)
                .buildGraph();
    }

    private void fillGraph(SimpleWeightedGraph<String, DefaultWeightedEdge> graph) {
        this.addVertexs(graph);
        this.addEdges(graph);
    }

    private void addVertexs(SimpleWeightedGraph<String, DefaultWeightedEdge> graph) {
        AIRPORTS.forEach(graph::addVertex);
    }

    private void addEdges(SimpleWeightedGraph<String, DefaultWeightedEdge> graph) {
        this.addEdge(graph, "DUB", "LHR", 1);
        this.addEdge(graph, "DUB", "CDG", 2);
        this.addEdge(graph, "CDG", "BOS", 6);
        this.addEdge(graph, "CDG", "BKK", 9);
        this.addEdge(graph, "ORD", "LAS", 2);
        this.addEdge(graph, "LHR", "NYC", 5);
        this.addEdge(graph, "NYC", "LAS", 3);
        this.addEdge(graph, "BOS", "LAX", 4);
        this.addEdge(graph, "LHR", "BKK", 9);
        this.addEdge(graph, "BKK", "SYD", 11);
        this.addEdge(graph, "LAX", "LAS", 2);
        this.addEdge(graph, "DUB", "ORD", 6);
        this.addEdge(graph, "LAX", "SYD", 13);
        this.addEdge(graph, "LAS", "SYD", 14);
    }

    private void addEdge(SimpleWeightedGraph<String, DefaultWeightedEdge> graph, String source, String dest, Integer distance) {
        DefaultWeightedEdge e = new DefaultWeightedEdge();
        graph.addEdge(source, dest, e);
        graph.setEdgeWeight(e, distance);
    }
}
