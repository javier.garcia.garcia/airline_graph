package com.boxever.challenge.airline.model;

import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.List;


public class AirlinePath {

    private GraphPath<String, DefaultWeightedEdge> path;

    public AirlinePath(GraphPath<String, DefaultWeightedEdge> path) {
        this.path = path;
    }

    public boolean exists() {
        return null != path;
    }

    @Override
    public String toString() {
        StringBuilder acc = new StringBuilder();
        List<String> airports = path.getVertexList();
        for(int i=0; i<airports.size()-1; i++) {
            appendStep(acc, airports.get(i), airports.get(i+1));
        }
        acc.append("Time: " + (int)path.getWeight());
        return acc.toString();
    }

    private void appendStep(StringBuilder path, String source, String dest) {
        path
            .append(source)
            .append(" -- ")
            .append(dest)
            .append("(")
            .append(getDistance(source, dest))
            .append(")\n");
    }

    private Integer getDistance(String source, String dest) {
        DefaultWeightedEdge e = this.path.getGraph().getEdge(source, dest);
        return (int)this.path.getGraph().getEdgeWeight(e);
    }
}
