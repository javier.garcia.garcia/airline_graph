package com.boxever.challenge.airline.exceptions;

public class InputException extends RuntimeException {
    public InputException(String message) {
        super(message);
    }
}
