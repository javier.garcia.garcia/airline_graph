package com.boxever.challenge.airline;

import com.boxever.challenge.airline.exceptions.InputException;
import com.github.blindpirate.extensions.CaptureSystemOutput;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AirlineApplicationAcceptanceTest {
    @Test
    @CaptureSystemOutput
    public void testShouldGetShortestPath(CaptureSystemOutput.OutputCapture outputCapture) {
        AirlineApplication.main(new String [] { "--source=DUB", "--dest=LHR" });
        outputCapture.expect(containsString("DUB -- LHR(1)"));
        outputCapture.expect(containsString("Time: 1"));
    }


    @Test
    public void testShouldFailWhenWrongSource() {
        Exception exception = assertThrows(InputException.class, () ->
                new AirlineApplication("NOT_VALID", "DUB", null));
        String expectedMessage = "Source or destination unknowns";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

    @Test
    public void testShouldFailWhenWrongDest() {
        Exception exception = assertThrows(InputException.class, () ->
                new AirlineApplication("DUB", "NOT_VALID", null));
        String expectedMessage = "Source or destination unknowns";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
    }

}
