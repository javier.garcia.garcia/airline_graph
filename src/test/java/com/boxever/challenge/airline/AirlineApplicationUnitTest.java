package com.boxever.challenge.airline;

import com.boxever.challenge.airline.algorithms.ShortestPathFinder;
import com.boxever.challenge.airline.exceptions.InputException;
import com.boxever.challenge.airline.model.AirlinePath;
import com.github.blindpirate.extensions.CaptureSystemOutput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class AirlineApplicationUnitTest {
    private ShortestPathFinder finderMocked;
    private AirlinePath pathMocked;
    private AirlineApplication underTest;

    @BeforeEach
    public void setUp() {
        finderMocked = Mockito.mock(ShortestPathFinder.class);
        pathMocked = Mockito.mock(AirlinePath.class);
    }

    @Test
    @CaptureSystemOutput
    public void shouldReturnValidPath(CaptureSystemOutput.OutputCapture outputCapture) throws Exception{
        String source = "DUB";
        String dest = "LAX";
        Mockito.when(finderMocked.getShortestPath(source, dest)).thenReturn(pathMocked);
        Mockito.when(pathMocked.exists()).thenReturn(true);
        underTest = new AirlineApplication(source, dest, this.finderMocked);
        Mockito.when(finderMocked.getShortestPath(source, dest)).thenReturn(pathMocked);
        Mockito.when(pathMocked.exists()).thenReturn(true);
        Mockito.when(pathMocked.toString()).thenReturn("Mocked string");

        underTest.run();
        Mockito.verify(finderMocked).getShortestPath(source,dest);
        Mockito.verify(pathMocked).exists();
        outputCapture.expect(containsString("Mocked string"));
    }

    @Test
    @CaptureSystemOutput
    public void shouldReturnPathNotExists(CaptureSystemOutput.OutputCapture outputCapture) throws Exception{
        String source = "DUB";
        String dest = "LAX";
        Mockito.when(finderMocked.getShortestPath(source, dest)).thenReturn(pathMocked);
        Mockito.when(pathMocked.exists()).thenReturn(true);
        underTest = new AirlineApplication(source, dest, this.finderMocked);
        Mockito.when(finderMocked.getShortestPath(source, dest)).thenReturn(pathMocked);
        Mockito.when(pathMocked.exists()).thenReturn(false);


        underTest.run();
        Mockito.verify(finderMocked).getShortestPath(source,dest);
        Mockito.verify(pathMocked).exists();
        outputCapture.expect(containsString("No path between DUB and LAX"));
    }

    @Test
    public void testShouldFailWhenWrongSource() {
        Exception exception = assertThrows(InputException.class, () ->
                new AirlineApplication("NOT_VALID", "DUB", this.finderMocked));
        String expectedMessage = "Source or destination unknowns";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
        Mockito.verifyNoInteractions(finderMocked, pathMocked);
    }

    @Test
    public void testShouldFailWhenWrongDest() {
        Exception exception = assertThrows(InputException.class, () ->
                new AirlineApplication("DUB", "NOT_VALID", this.finderMocked));
        String expectedMessage = "Source or destination unknowns";
        String actualMessage = exception.getMessage();

        assertThat(actualMessage, equalTo(expectedMessage));
        Mockito.verifyNoInteractions(finderMocked, pathMocked);
    }
}
