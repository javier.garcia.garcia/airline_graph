package com.boxever.challenge.airline.model;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class AirlineGraphTest {

    @Test
    public void shouldGetRightGraphForDUB() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("DUB", "LHR");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(1d));
        e = graph.getGraph().getEdge("DUB", "CDG");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(2d));
        e = graph.getGraph().getEdge("DUB", "ORD");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(6d));
    }

    @Test
    public void shouldGetRightGraphForCDG() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("CDG", "BOS");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(6d));
        e = graph.getGraph().getEdge("CDG", "BKK");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(9d));
    }

    @Test
    public void shouldGetRightGraphForORD() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("ORD", "LAS");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(2d));
    }

    @Test
    public void shouldGetRightGraphForLHR() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("LHR", "NYC");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(5d));
        e = graph.getGraph().getEdge("LHR", "BKK");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(9d));
    }

    @Test
    public void shouldGetRightGraphForNYC() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("NYC", "LAS");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(3d));
    }

    @Test
    public void shouldGetRightGraphForBOS() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("BOS", "LAX");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(4d));
    }

    @Test
    public void shouldGetRightGraphForBKK() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("BKK", "SYD");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(11d));
    }

    @Test
    public void shouldGetRightGraphForLAX() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("LAX", "LAS");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(2d));
        e = graph.getGraph().getEdge("LAX", "SYD");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(13d));
    }

    @Test
    public void shouldGetRightGraphForLAS() {
        AirlineGraph graph = new AirlineGraph();
        DefaultWeightedEdge e = graph.getGraph().getEdge("LAS", "SYD");
        assertThat(e, notNullValue());
        assertThat(graph.getGraph().getEdgeWeight(e), equalTo(14d));
    }
}
