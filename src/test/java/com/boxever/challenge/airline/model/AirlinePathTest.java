package com.boxever.challenge.airline.model;

import com.boxever.challenge.airline.model.AirlinePath;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AirlinePathTest {

    @Test
    public void shouldReturnRightFormat() {
        GraphPath<String, DefaultWeightedEdge> graphPath = getMockedGraph();
        AirlinePath path = new AirlinePath(graphPath);
        assertThat(path.toString(), equalTo("A -- B(1)\nB -- C(1)\nTime: 1"));
    }

    @Test
    public void shouldReturnNotExists() {
        AirlinePath path = new AirlinePath(null);
        assertThat(path.exists(), equalTo(false));
    }

    @Test
    public void shouldReturnExists() {
        AirlinePath path = new AirlinePath(getMockedGraph());
        assertThat(path.exists(), equalTo(true));
    }

    private GraphPath getMockedGraph() {
        GraphPath<String, DefaultWeightedEdge> graphPath = Mockito.mock(GraphPath.class);
        Graph graph = Mockito.mock(Graph.class);
        DefaultWeightedEdge edge = Mockito.mock(DefaultWeightedEdge.class);
        Mockito.when(graph.getEdgeWeight(edge)).thenReturn(1d);
        Mockito.when(graph.getEdge("A", "B")).thenReturn(edge);
        Mockito.when(graph.getEdge("B", "C")).thenReturn(edge);
        Mockito.when(graphPath.getVertexList()).thenReturn(List.of("A", "B", "C"));
        Mockito.when(graphPath.getGraph()).thenReturn(graph);
        Mockito.when(graphPath.getWeight()).thenReturn(1d);
        return graphPath;
    }
}
