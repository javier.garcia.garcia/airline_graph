# Airline Shortest Path calculator

## Requisites
* Docker installed

## How to build the app
Run

``make build``

It will generate a docker image containing spring boot application to run the path finder

## How to run the app
After building it, you can run it by doing

``make run``

It will run the app using the default configuration
* Source airport: DUB
* Destination airport: LHR

Output
````
╰─$ make run SOURCE=LAX DEST=SYD                                                                                                                                                                                                        2 ↵
    docker run boxever/airline_graph --source=LAX --dest=SYD

      .   ____          _            __ _ _
     /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
    ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
     \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
      '  |____| .__|_| |_|_| |_\__, | / / / /
     =========|_|==============|___/=/_/_/_/
     :: Spring Boot ::        (v2.2.6.RELEASE)

    2021-01-28 10:11:29.044  INFO 1 --- [           main] c.b.c.airline.AirlineApplication         : Starting AirlineApplication v1.0.0 on a94dee0eed67 with PID 1 (/airline-graph.jar started by root in /)
    2021-01-28 10:11:29.051  INFO 1 --- [           main] c.b.c.airline.AirlineApplication         : No active profile set, falling back to default profiles: default
    2021-01-28 10:11:30.757  INFO 1 --- [           main] c.b.c.airline.AirlineApplication         : Started AirlineApplication in 2.863 seconds (JVM running for 3.986)
    LAX -- SYD(13)
    Time: 13

````

## Bases of the project
* Project based on Java 14 & Spring Boot 2
* Code desinged and developed following the SOLID principles and clean code rules
* Developed following TDD rules
* 100% test coverage
* 100% mutation test coverage
* PMD as checkstyle of the project
* Docker as base to build an executable
* Using GitlabCI as Continous Integration
* Using precommit to avoid commit changes without checking PMD checksytle

### Testing
The code has been developed following TDD rules (https://martinfowler.com/bliki/TestDrivenDevelopment.html)
In order to check the right behaviour of the app, the coverage of test has been set to 100% (https://gitlab.com/javier.garcia.garcia/rock_scissors_papper_game/-/blob/master/pom.xml#L127)
The project is using Mutation testing to increase the coverage (https://pitest.org)

You can get the test report in gitlab ci:
https://gitlab.com/javier.garcia.garcia/airline_graph/-/jobs/992136268/artifacts/browse

## Use of precommit
In order to avoid committing wrong changes in code, in terms of style, I've enabled the precommit in git
The check will be executed only when any java file is modified
To do that I've change the precommit command:
```
#!/bin/bash -e
function get_module() {
  local path=$1;
  while true; do
    path=$(dirname $path);
    if [ -f "$path/pom.xml" ]; then
      echo "$path";
      return;
    elif [[ "./" =~ "$path" ]]; then
      return;
    fi
  done
}

modules=();

for file in $(git diff --name-only --cached \*.java); do
  module=$(get_module "$file");
  if [ "" != "$module" ] \
      && [[ ! " ${modules[@]} " =~ " $module " ]]; then
    modules+=("$module");
  fi
done;

if [ ${#modules[@]} -eq 0 ]; then
  exit;
fi

modules_arg=$(printf ",%s" "${modules[@]}");
modules_arg=${modules_arg:1};

export MAVEN_OPTS="-client
  -XX:+TieredCompilation
  -XX:TieredStopAtLevel=1
  -Xverify:none";

mvn -q -pl "$modules_arg" pmd:check;
```