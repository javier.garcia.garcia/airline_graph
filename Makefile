DOCKER_IMAGE=$(shell echo $${DOCKER_IMAGE:-boxever/airline_graph})
SOURCE=$(shell echo $${SOURCE:-DUB})
DEST=$(shell echo $${DEST:-SYD})

lint:
	docker build --target LINTER -t $(DOCKER_IMAGE) .

build:
	docker build -t $(DOCKER_IMAGE) .

run:
	docker run $(DOCKER_IMAGE) --source=${SOURCE} --dest=${DEST}

